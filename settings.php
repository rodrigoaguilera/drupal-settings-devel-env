<?php

/**
 * Enable local development services.
 */
if (getenv('DRUPAL_SKIP_DEVEL_SERVICES') !== 'TRUE') {
  $settings['container_yamls'][] = __DIR__ . '/services.yml';
}


/**
 * Show all error messages, with backtrace information.
 *
 * In case the error level could not be fetched from the database, as for
 * example the database connection failed, we rely only on this value.
 */
$config['system.logging']['error_level'] = getenv('DRUPAL_ERROR_LEVEL') ?: 'verbose';

/**
 * Disable CSS and JS aggregation.
 */
$config['system.performance']['css']['preprocess'] = getenv('DRUPAL_PREPROCESS_CSS') === 'TRUE';
$config['system.performance']['js']['preprocess'] = getenv('DRUPAL_PREPROCESS_JS') === 'TRUE';

/**
 * Disable the render cache.
 *
 * Note: you should test with the render cache enabled, to ensure the correct
 * cacheability metadata is present. However, in the early stages of
 * development, you may want to disable it.
 *
 * This setting disables the render cache by using the Null cache back-end
 * defined by the development.services.yml file above.
 *
 * Only use this setting once the site has been installed.
 */
if (getenv('DRUPAL_NULL_RENDER_CACHE')) {
  $settings['cache']['bins']['render'] = 'cache.backend.null';
}

/**
 * Disable caching for migrations.
 *
 * Only store migrations in memory and not in the
 * database. This makes it easier to develop custom migrations.
 */
if (getenv('DRUPAL_MEMORY_MIGRATION_CACHE')) {
  $settings['cache']['bins']['discovery_migration'] = 'cache.backend.memory';
}

/**
 * Disable Internal Page Cache.
 *
 * Note: you should test with Internal Page Cache enabled, to ensure the correct
 * cacheability metadata is present. However, in the early stages of
 * development, you may want to disable it.
 *
 * This setting disables the page cache by using the Null cache back-end
 * defined by the development.services.yml file above.
 *
 * Only use this setting once the site has been installed.
 */
if (getenv('DRUPAL_NULL_PAGE_CACHE')) {
  $settings['cache']['bins']['page'] = 'cache.backend.null';
}

/**
 * Disable Dynamic Page Cache.
 *
 * Note: you should test with Dynamic Page Cache enabled, to ensure the correct
 * cacheability metadata is present (and hence the expected behavior). However,
 * in the early stages of development, you may want to disable it.
 */
if (getenv('DRUPAL_NULL_DYNAMIC_PAGE_CACHE')) {
  $settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
}

/**
 * Allow test modules and themes to be installed.
 *
 * Drupal ignores test modules and themes by default for performance reasons.
 * During development it can be useful to install test extensions for debugging
 * purposes.
 */
if (getenv('DRUPAL_EXTENSION_DISCOVERY_SCAN_TESTS')) {
  $settings['extension_discovery_scan_tests'] = TRUE;
}

/**
 * Enable access to rebuild.php.
 *
 * This setting can be enabled to allow Drupal's php and database cached
 * storage to be cleared via the rebuild.php page. Access to this page can also
 * be gained by generating a query string from rebuild_token_calculator.sh and
 * using these parameters in a request to rebuild.php.
 */
$settings['rebuild_access'] = getenv('DRUPAL_DENY_REBUILD_ACCESS') === 'TRUE';

/**
 * Skip file system permissions hardening.
 *
 * The system module will periodically check the permissions of your site's
 * site directory to ensure that it is not writable by the website user. For
 * sites that are managed with a version control system, this can cause problems
 * when files in that directory such as settings.php are updated, because the
 * user pulling in the changes won't have permissions to modify files in the
 * directory.
 */
$settings['skip_permissions_hardening'] = getenv('DRUPAL_ENABLE_PERMISSIONS_HARDENING') !== 'TRUE';

/**
 * Exclude modules from configuration synchronization.
 *
 * On config export sync, no config or dependent config of any excluded module
 * is exported. On config import sync, any config of any installed excluded
 * module is ignored. In the exported configuration, it will be as if the
 * excluded module had never been installed. When syncing configuration, if an
 * excluded module is already installed, it will not be uninstalled by the
 * configuration synchronization, and dependent configuration will remain
 * intact. This affects only configuration synchronization; single import and
 * export of configuration are not affected.
 *
 * Drupal does not validate or sanity check the list of excluded modules. For
 * instance, it is your own responsibility to never exclude required modules,
 * because it would mean that the exported configuration can not be imported
 * anymore.
 *
 * This is an advanced feature and using it means opting out of some of the
 * guarantees the configuration synchronization provides. It is not recommended
 * to use this feature with modules that affect Drupal in a major way such as
 * the language or field module.
 */
$settings['config_exclude_modules'] = getenv('DRUPAL_CONFIG_EXCLUDE_MODULES') ?
  explode(',', getenv('DRUPAL_CONFIG_EXCLUDE_MODULES')) :
  // Ensure that no configuration depends on modules defined here. For example,
  // when the devel module is excluded and the authenticated role has devel permission
  // the whole role will be excluded from configuration resulting in a broken site.
  // For this example is better to use config_split patches.
  [];

$config['field_ui.settings']['field_prefix'] = getenv('DRUPAL_FIELD_UI_PREFIX_EMPTY') ? '' :
  (getenv('DRUPAL_FIELD_UI_PREFIX') ?: 'field_');

$config['contact.settings']['flood']['limit'] = getenv('DRUPAL_CONTACT_FLOOD_LIMIT') ?: 500;
